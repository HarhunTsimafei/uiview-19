//
//  ViewController.m
//  UIView - 19 les
//
//  Created by Timofei Harhun on 14.03.15.
//  Copyright (c) 2015 timofei. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property(weak, nonatomic) UIView* whiteColor;
@property(assign, nonatomic) CGFloat size;

- (UIView *) createViewCGRect: (CGRect)rect superView: (UIView *)parent backgroungColor: (UIColor*)color;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGFloat viewWidth   = self.view.bounds.size.width;
    CGFloat viewHeight  = self.view.bounds.size.height;
    
    UIView* blackSquare = [self createViewCGRect:CGRectMake(0, viewHeight/2-viewWidth/2, viewWidth, viewWidth) superView:self.view backgroungColor: [UIColor blackColor]];
    
    blackSquare.autoresizingMask =  UIViewAutoresizingFlexibleLeftMargin | UIViewAutoresizingFlexibleRightMargin |
                                    UIViewAutoresizingFlexibleTopMargin | UIViewAutoresizingFlexibleBottomMargin;

    UIView* whiteColor = [self createViewCGRect:CGRectMake(blackSquare.bounds.origin.x+4, blackSquare.bounds.origin.y+4, blackSquare.bounds.size.width-8, blackSquare.bounds.size.height-8) superView:blackSquare backgroungColor:[UIColor whiteColor]];
    
    CGFloat sizeSquare = (whiteColor.bounds.size.width - 8)/8;
    self.size=sizeSquare;
    CGFloat x = 4,
            y = 4,
            width = sizeSquare,
            height = sizeSquare;
    
    for (int i=1, j=0; i<=32; i++) {
        [self createViewCGRect:CGRectMake(x, y, width, height) superView: whiteColor backgroungColor:[UIColor blackColor]];
        
        if (j>= 0 && j <= 2) {
            [self createViewCGRect:CGRectMake(x+4, y+4, width-8, height-8) superView: whiteColor backgroungColor:[UIColor redColor]];
        }
        
        if (j >= 5 && j <= 7) {
           [self createViewCGRect:CGRectMake(x+4, y+4, width-8, height-8) superView: whiteColor backgroungColor:[UIColor whiteColor]];
        }
        
        if (i%4 != 0) {
            x += 2*sizeSquare;
        } else {
            j++;
            (j%2 != 0) ? (x = 4+sizeSquare) : (x=4);
            y += sizeSquare;
        }
    }
    
    self.whiteColor = whiteColor;

}

- (NSUInteger) supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAll;
}

- (void) viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator{
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
     UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];

    [self updateLayoutWithOrientation:orientation];
}

- (void)updateLayoutWithOrientation: (UIInterfaceOrientation) orinentation{
    
    NSArray* arrayMiniBlackSquare = self.whiteColor.subviews;
    UIColor* backgroundColor = [UIColor blackColor];
   
    switch (orinentation) {
        case UIInterfaceOrientationLandscapeLeft:
            backgroundColor = [UIColor yellowColor];
            break;
        case UIInterfaceOrientationLandscapeRight:
            backgroundColor = [UIColor blueColor];
            break;
        case UIInterfaceOrientationPortrait:
            backgroundColor = [UIColor blackColor];
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
            backgroundColor = [UIColor greenColor];
            break;
            
        default:
            break;
    }
    
    for (UIView* view in arrayMiniBlackSquare) {
        if (view.bounds.size.height == (self.whiteColor.frame.size.width-8)/8) {
            [self changeColorView:view backgroungColor:backgroundColor];
           
        } else {
            [self.whiteColor bringSubviewToFront:view];
        }
    }

    __block NSMutableArray* randArr = [[NSMutableArray alloc] init];
    
    [randArr addObject:[NSNumber numberWithInteger:arrayMiniBlackSquare.count+1]];
    
    
    NSInteger (^addNum)()= ^NSInteger {
        
        NSInteger num = arc4random()%arrayMiniBlackSquare.count;
        int i=0;
        NSNumber* obj = [[NSNumber alloc] init];
        UIView* view = [[UIView alloc] init];
        
        do{
            
            obj = randArr[i];
            view = arrayMiniBlackSquare[num];
            i++;
            
            if ( view.bounds.size.width != self.size-8 || [obj integerValue] == num) {
                num = arc4random()%arrayMiniBlackSquare.count;
                i=0;
            }
            if (i == randArr.count) {
                [randArr addObject:[NSNumber numberWithInteger:num]];
                break;
            }
            
        }
        while(true);
        return num;
    };
    
    for (int j=0; j<12; j++) {
        
        NSInteger one = addNum();
        NSInteger two = addNum();
        
        [UIView animateWithDuration:3 animations:^{
            UIView* viewOne = [[UIView alloc] init];
            viewOne=arrayMiniBlackSquare[one];
            CGRect rectOne = viewOne.frame;
            UIView* viewTwo = [[UIView alloc] init];
            viewTwo=arrayMiniBlackSquare[two];
            viewOne.frame=viewTwo.frame;
            viewTwo.frame=rectOne;
          
        }];
        
    }
    
    
}


#pragma mark createView

- (UIView *) createViewCGRect: (CGRect)rect superView: (UIView *)parent backgroungColor: (UIColor*)color {
    
    
    UIView* view = [[UIView alloc] initWithFrame: rect];
    view.backgroundColor = color;
    [parent addSubview:view];
    
    return view;
}

#pragma mark changeColor

- (void) changeColorView: (UIView*) view backgroungColor: (UIColor*) color {
    
    view.backgroundColor = color;
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
